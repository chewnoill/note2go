#!/usr/bin/python
'''
Created on Aug 29, 2016

@author: will

1. --month-view --date 2016-09-01 --duplex --sync-to-drive
2. --process-image p1.png p2.png --sync-to-drive
3. --sync-from-email --sync-to-drive

'''

from __future__ import print_function

from requests_oauthlib import OAuth2Session
from note.page import MonthPage, EmptyPage
from apiclient.http import MediaFileUpload

import httplib2
import os

from apiclient import discovery
import oauth2client
import oauth2client.file
from oauth2client import client
from oauth2client import tools
from pprint import pprint
from note.decode import Decode
import apiclient
import yaml
import datetime
import dateutil
import base64
import uuid
import email

CREDENTIALS_FILE = os.environ.get('CREDENTIALS_FILE', None)
CONFIG_FILE = os.environ.get('CONFIG_FILE', '../config.yaml')
CLIENT_SECRET_FILE = os.environ.get(
    'CLIENT_SECRET_FILE',
    '../client_secret.json')

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser])
    flags.add_argument('--date')
    flags.add_argument('--pages', type=int)
    flags.add_argument('--process-image', nargs='+')
    flags.add_argument('--sync-from-email', action="store_true")
    flags.add_argument('--month-view', action="store_true")
    flags.add_argument('--empty-view', action="store_true")
    flags.add_argument('--duplex', action="store_true")
    flags.add_argument('--sync-to-drive', action="store_true")

    flags = flags.parse_args()
    flags.noauth_local_webserver = True
except ImportError:
    flags = None

params = yaml.safe_load(open(CONFIG_FILE))

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPES = ['https://www.googleapis.com/auth/calendar.readonly',
          'https://www.googleapis.com/auth/gmail.readonly',
          'https://www.googleapis.com/auth/gmail.modify',
          'https://www.googleapis.com/auth/drive']

APPLICATION_NAME = 'Google Calendar API Python Quickstart'


def main():

    pprint(params)

    """Shows basic usage of the Google Calendar API.

    Creates a Google Calendar API service object and outputs a list of the next
    10 events on the user's calendar.
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())

    #now = datetime.date(flags.year, flags.month, 1)
    images = {}
    email_images = {}
    gmail_service = None
    unused = False
    if (flags.month_view or flags.empty_view):
        images = print_page(params)
        unused = True
    elif flags.sync_from_email:
        gmail_service = discovery.build('gmail', 'v1', http=http)
        email_images = sync_from_email(gmail_service)
        for email_image in email_images:
            image_path = email_images[email_image]
            img_dict = Decode(image_path)
            for img in img_dict:
                t_dict = img_dict[img]
                images[base64.b64decode(img)] = img_dict[img]
        pprint(images)

    elif flags.process_image:
        image_paths = flags.process_image
        for image_path in image_paths:
            img_dict = Decode(image_path)

            for img in img_dict:
                images[base64.b64decode(img)] = {'image': img_dict[img],
                                                 'image_path': image_path}

            pprint(images)

    if flags.sync_to_drive:
        print('syncing to drive...')
        gdrive_service = discovery.build('drive', 'v3', http=http)

        sync_to_drive(gdrive_service, images, unused)

        if flags.sync_from_email:
            print('resyncing to gmail...')
            email_ids = [x[0] for x in email_images.keys()]
            read_messages(gmail_service, email_ids)


def read_messages(gmail_service, email_ids):
    for msg_id in email_ids:
        print('removing INBOX from %s' % msg_id)
        try:
            msg_labels = {'removeLabelIds': ['INBOX'], 'addLabelIds': []}
            message = gmail_service.users().messages().modify(userId='me', id=msg_id,
                                                              body=msg_labels).execute()

            label_ids = message['labelIds']

            print('Message ID: %s - With Label IDs %s' % (msg_id, label_ids))
            return message
        except Exception as error:
            print('An error occurred: %s' % error)


def sync_to_drive(gdrive_service, images, unused):
    pprint(images)

    # Find folder ID, or create
    folder_name = params['drive']['folder']
    qry = '''
    mimeType = 'application/vnd.google-apps.folder' and
    name = '%s'
    ''' % folder_name
    response = gdrive_service.files().list(q=qry).execute()
    if len(response['files']) == 0:
        # create folder
        body = {'mimeType': 'application/vnd.google-apps.folder',
                'name': folder_name}
        response = gdrive_service.files().create(body=body).execute()
        folder_id = response['id']
    else:
        folder_id = response['files'][0]['id']
    print(folder_id)
    # find/create unused folder
    qry = '''
    mimeType = 'application/vnd.google-apps.folder' and
    name = '%s' and '%s' in parents
    ''' % ('unused', folder_id)
    response = gdrive_service.files().list(q=qry).execute()
    if len(response['files']) == 0:
        # create folder
        body = {'mimeType': 'application/vnd.google-apps.folder',
                'name': 'unused',
                'parents': [folder_id]}
        response = gdrive_service.files().create(body=body).execute()
        unused_folder_id = response['id']
    else:
        unused_folder_id = response['files'][0]['id']
    print(unused_folder_id)

    qry = '''
    '%s' in parents and '%s' in parents
    ''' % (folder_id, unused_folder_id)
    response = gdrive_service.files().list(q=qry,
                                           orderBy='createdTime').execute()
    print(response)
    # build list of current images
    found_images = []
    for f in response['files']:
        found_images += [f['id']]
    if unused:
        removeParents = None  # %s' % folder_id
        addParents = unused_folder_id
    else:
        removeParents = unused_folder_id
        addParents = folder_id
    for image_id in images:
        image_path = images[image_id]['image_path']
        image_name = images[image_id].get('image_name', image_id)
        media_body = MediaFileUpload(image_path, resumable=True)

        if unused:
            body = {
                'name': image_name,
                'id': image_id,
                'parents': [addParents]
            }
            pprint(body)
            file = gdrive_service.files().create(
                body=body,
                media_body=media_body).execute()
            pprint(file)
            print('File ID: %s' % file['id'])
        else:
            f = gdrive_service.files().update(
                fileId=image_id,
                media_body=media_body,
                addParents=addParents,
                removeParents=removeParents).execute()
            pprint(f)
            print('Updated File ID: %s' % f['id'])


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    credential_dir = os.path.join('.', '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')

    if CREDENTIALS_FILE:
        credential_path = CREDENTIALS_FILE

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        print('Storing credentials to ' + credential_path)
    return credentials


def getDateRange(service, calendarId, start, end):

    ret = []
    event_set = {}
    more = True
    while more:
        print('getting date: %s' % start.isoformat())
        eventsResult = service.events().list(
            calendarId=calendarId,
            timeMin=start.isoformat() + 'Z', maxResults=5, singleEvents=True,
            orderBy='startTime').execute()
        events = eventsResult.get('items', [])
        for event in events:
            start_date = event['start'].get(
                'date',
                event['start'].get('dateTime'))
            start_date_time = dateutil.parser.parse(start_date)
            if int(start_date_time.strftime("%s")) > int(end.strftime("%s")):
                more = False
                break
            if event['id'] not in event_set:
                event_set[event['id']] = event
            else:
                print('duplicate event found!!!')
            start = datetime.datetime(year=start_date_time.year,
                                      month=start_date_time.month,
                                      day=start_date_time.day)

    return event_set.values()


def ListLabels(service, user_id):
    """Get a list all labels in the user's mailbox.

      Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.

      Returns:
        A list all Labels in the user's mailbox.
      """
    try:
        response = service.users().labels().list(userId=user_id).execute()
        labels = response['labels']
        return labels
    except apiclient.errors.HttpError as error:
        print('An error occurred: %s' % error)
        raise


def ListMessagesWithLabels(service, user_id, label_ids=[]):
    """List all Messages of the user's mailbox with label_ids applied.

      Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.
        label_ids: Only return Messages with these labelIds applied.

      Returns:
        List of Messages that have all required Labels applied. Note that the
        returned list contains Message IDs, you must use get with the
        appropriate id to get the details of a Message.
    """
    try:
        response = service.users().messages().list(userId=user_id,
                                                   labelIds=label_ids).execute()
        messages = []
        if 'messages' in response:
            pprint(response)
            messages.extend(response['messages'])

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = service.users().messages().list(userId=user_id,
                                                       labelIds=label_ids,
                                                       pageToken=page_token).execute()

            messages.extend(response['messages'])

        return messages
    except apiclient.errors.HttpError as error:
        print('An error occurred: %s' % error)


def GetMessage(service, user_id, msg_id):
    """Get a Message with given ID.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The ID of the Message required.

    Returns:
      A Message.
    """
    try:
        message = service.users().messages().get(
            userId=user_id,
            id=msg_id).execute()

        print('Message snippet: %s' % message['snippet'])

        return message
    except apiclient.errors.HttpError as error:
        print('An error occurred: %s' % error)


def GetMimeMessage(service, user_id, msg_id):
    """Get a Message and use it to create a MIME Message.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The ID of the Message required.

    Returns:
      A MIME Message, consisting of data from Message.
    """
    try:
        message = service.users().messages().get(userId=user_id, id=msg_id,
                                                 format='raw').execute()

        print('Message snippet: %s' % message['snippet'])

        msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))

        mime_msg = email.message_from_string(msg_str)

        return mime_msg
    except apiclient.errors.HttpError as error:
        print('An error occurred: %s' % error)


def GetAttachments(service, user_id, msg_id, store_dir):
    """Get and store attachment from Message with given id.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: ID of Message containing attachment.
      store_dir: The directory used to store attachments.
    """

    ret = {}
    try:
        message = service.users().messages().get(
            userId=user_id,
            id=msg_id).execute()

        labels = params['gmail']['labels']['secondary']
        skip = True
        for labelid in message['labelIds']:
            if labelid in labels:
                skip = False

        if skip:
            return ret
        for part in message['payload']['parts']:
            if (part['filename']):
                pprint(part)
                attachmentId = part['body']['attachmentId']
                attachment = service.users().messages().attachments().get(
                    userId=user_id,
                    messageId=msg_id,
                    id=attachmentId).execute()

                file_data = base64.urlsafe_b64decode(attachment['data']
                                                     .encode('UTF-8'))

                path = os.path.join(store_dir, part['filename'])
                ret[(msg_id, attachmentId)] = path
                f = open(path, 'w')
                f.write(file_data)
                f.close()

    except apiclient.errors.HttpError as error:
        print('An error occurred: %s' % error)

    return ret


def sync_from_email(service):
    label_filter = params['gmail']['labels']['primary']

    labels = ListLabels(service, 'me')
    tmp_dir = os.path.join('archive', 'temp')
    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)
    scan_label = None
    label_dict = {}
    for label in labels:
        if label['name'] == label_filter:
            label_filter_id = label['id']

    images = {}
    if label_filter_id:
        # get messages
        messages = ListMessagesWithLabels(
            service, 'me', [
                label_filter_id, 'INBOX'])

        for message in messages:

            m = GetAttachments(service, 'me', message['id'], tmp_dir)
            for a, b in m:
                images[a, b] = m[a, b]

            body = {'removeLabelIds': ['INBOX']}
            response = service.users().messages().modify(userId='me', id=message['id'],
                                                         body=body).execute()
            pprint(response)

    return(images)


def print_page(params):
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('calendar', 'v3', http=http)
    drive_service = discovery.build('drive', 'v3', http=http)
    IDs = drive_service.files().generateIds(count=2).execute()
    name = 'Untitled'
    pprint(IDs)
    now = datetime.datetime.now()
    ID = uuid.uuid4()
    ret = {}
    p1 = None
    p2 = None
    pages = []
    if flags.month_view:

        date = dateutil.parser.parse(flags.date)

        start_date = date - datetime.timedelta(days=7)
        end_date = date.replace(
            day=28) + datetime.timedelta(days=10)  # this will never fail

        events = []
        for cal in params['calendars']:

            events += getDateRange(
                service,
                cal['calendarId'],
                start_date,
                end_date)
        month = MonthPage(date.month, date.year)
        qr_data = '%s' % IDs['ids'][0]
        month.qr_data = base64.b64encode(qr_data)
        month.qr_pos = 'br'
        for event in events:
            month.addEvent(event)
            start = event['start'].get('dateTime', event['start'].get('date'))
            print(start, event['summary'])
        # pprint(events)
        month.draw()
        file_name = 'p1.png'
        out = open(file_name, 'w')
        month._image.save(out, "PNG")
        out.close()
        name = '%04d%02d%02d' % (date.year, date.month, date.day)
        ret[qr_data] = {'image': month._image,
                        'image_path': file_name,
                        'image_name': '%s page1.png' % (name)}
        pages += ['p1.png']
        p1 = month._image
    else:
        empty_page = EmptyPage()
        qr_data = '%s' % IDs['ids'][0]
        empty_page.qr_data = base64.b64encode(qr_data)
        empty_page.qr_pos = 'br'
        empty_page.draw()
        file_name = 'p1.png'
        out = open(file_name, 'w')
        empty_page._image.save(out, "PNG")
        out.close()
        ret[qr_data] = {'image': empty_page._image,
                        'image_path': file_name}
        if name:
            ret[qr_data]['image_name'] = '%s page1.png' % name
        p1 = empty_page._image
        pages += [file_name]
    if flags.duplex:

        empty_page = EmptyPage()
        qr_data = '%s' % IDs['ids'][1]
        empty_page.qr_data = base64.b64encode(qr_data)
        empty_page.qr_pos = 'bl'
        empty_page.draw()
        file_name = 'p2.png'
        out = open(file_name, 'w')
        empty_page._image.save(out, "PNG")
        out.close()
        p2 = empty_page._image
        ret[qr_data] = {'image': empty_page._image,
                        'image_path': file_name}
        if name:
            ret[qr_data]['image_name'] = '%s page2.png' % name
        pages += ['p2.png']
    # create ps file
    convertToPdf(pages)
    return ret


def convertToPdf(pages, filename='out.pdf'):
    from fpdf import FPDF
    pdf = FPDF(format='letter')
    for page in pages:
        pdf.add_page()
        pdf.image(page, 0, 0)
    path = os.path.join('archive', filename)
    pdf.output(path, "F")

if __name__ == '__main__':
    main()
