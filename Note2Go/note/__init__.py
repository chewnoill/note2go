
class Point:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, point):
        return Point(self.x + point.x,
                     self.y + point.y)

    def __sub__(self, point):
        return Point(self.x - point.x,
                     self.y - point.y)

    def to_point(self):
        return (self.x, self.y)

    def __mul__(self, point):

        return Point(self.x * point.x,
                     self.y * point.y)

    def __str__(self):
        return '(%s, %s)' % (self.x, self.y)