'''
Created on Aug 29, 2016

@author: will
'''
import unittest
import sys
from PIL import Image, ImageDraw, ImageFont
import datetime
import dateutil.parser

from pprint import pprint
import qrtools
from note import Point
from note.calendar import Day, Event


class EmptyPage:

    def __init__(self, title=None, qr_data=None):
        image = Image.open("Untitled.pgm")
        self._image = image
        self._size = image.size
        self._lines = []
        self._border = 10
        self.title = title
        self.qr_data = qr_data
        self.qr_pos = 'bl'
        self._font_file = 'Bariol_Bold.otf'

        self._top_left = Point(self._border, 45 + self._border)
        print(self._top_left)

    def draw(self):
        draw = ImageDraw.Draw(self._image)
        self.render(draw)
        del draw

    def _get_font(self, size=10):
        return ImageFont.truetype(self._font_file,
                                  size=size)

    def render(self, draw):
        if self.title:
            font = self._get_font(20)
            text_size = draw.textsize(self.title, font)
            title_point = Point(self._size[0] / 2, self._border)
            title_point -= Point(text_size[0] / 2, 0)
            draw.text(title_point.to_point(), self.title, font=font)

        if self.qr_data:
            qr = qrtools.QR(
                data=self.qr_data,
            )
            qr.encode('temp')
            print(qr.filename)
            img = Image.open(qr.filename)
            img.convert('1').save(qr.filename)
            img = Image.open(qr.filename)

            x = self._border  # left hand side
            y = self._border
            print(self.qr_pos)
            if self.qr_pos[0] == 'b':
                y = self._size[1] - self._border - img.size[1]  # bottom

            if self.qr_pos[1] == 'r':
                x = self._size[0] - self._border - img.size[0]  # bottom
            tl = Point(x, y)

            self._image.paste(img,
                              tl.to_point())


class MonthPage(EmptyPage):

    def __init__(self, month=None, year=None):
        EmptyPage.__init__(self)
        self.days = {}
        day_box_size = (min(self._size) - self._border * 2) / 7
        self.qr_data = 'month_view:%s-%2d,'
        self._day_box = Point(day_box_size, day_box_size)

        if (not month or
                not year):
            now = datetime.datetime.now()
            month = now.month
            year = now.year
        self.events = []

        date = datetime.date(year, month, 1)
        months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December']
        self.title = '%s %s' % (months[date.month - 1],
                                date.year)
        print(date.isocalendar())
        print('%s:%s' % (date.day, date.isoweekday()))

        s = date.isoweekday() - 1
        if s == 0:
            s = 7
        first_day = date - datetime.timedelta(days=s)
        print(first_day)

        next_month = date.replace(
            day=28) + datetime.timedelta(days=4)  # this will never fail
        next_month - datetime.timedelta(days=next_month.day)
        print(date)

        last_day = next_month - datetime.timedelta(days=1)
        s = 7 - last_day.isoweekday()
        if s == 0:
            s = 7
        last_day = last_day + \
            datetime.timedelta(days=s)
        print('first day: %s' % first_day)
        print('last day: %s' % last_day)
        num_days = last_day - first_day
        num_weeks = (num_days.days) / 7
        print('num weeks: %s' % num_weeks)
        print(date - datetime.timedelta(1))
        print(next_month)

        day = first_day
        self.days = {}
        for y in range(0, num_weeks + 1):
            row = y * day_box_size
            for x in range(0, 7):
                tl = self._top_left + \
                    Point(x * day_box_size, row)

                if day.month == date.month:
                    if day.isoweekday() in (1, 7):
                        d = Day(tl, self._day_box, day, color=0xee)
                    else:
                        d = Day(tl, self._day_box, day, color=0xff)
                else:
                    d = Day(tl, self._day_box, day, color=0xbb)
                self.days['%s-%02d-%02d' % (day.year, day.month, day.day)] = d
                day += datetime.timedelta(days=1)

    def addEvent(self, event):

        day_list = sorted(self.days.keys())

        start = event['start']
        pprint(start)
        start_date = start.get('date', start.get('dateTime'))

        print 'adding date %s' % start_date
        start_date_time = dateutil.parser.parse(start_date)
        start_date = '%s-%02d-%02d' % (start_date_time.year,
                                       start_date_time.month,
                                       start_date_time.day)
        # build timeframe

        end = event['end']
        end_date = end.get('date', start.get('dateTime'))
        end_date_time = dateutil.parser.parse(end_date)
        end_date = '%s-%02d-%02d' % (end_date_time.year,
                                     end_date_time.month,
                                     end_date_time.day)
        if start_date not in self.days:
            return
        day = self.days[start_date]
        if end_date not in day_list:
            end_date = day_list[-1]

        a = day_list.index(start_date)
        b = day_list.index(end_date)
        print('%s:%s' % (a, b))

        s = day_list.index(start_date) + 1
        e = max(day_list.index(end_date) + 1, s + 1)
        event_days = day_list[s:e]

        days = [self.days[d] for d in event_days]
        split_days = []
        week = []
        a_week = []
        for day in days:
            week += [day]
            if day.date.isoweekday() == 7:
                split_days += [week]
                week = []
        if len(week) > 0:
            split_days += [week]

        for days in split_days:
            evt = Event(event, days)
            self.events += [evt]

    def render(self, draw):
        EmptyPage.render(self, draw)

        font = self._get_font(10)
        headers = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday']

        for day in self.days.values():
            day.render(draw, font)
        for event in sorted(self.events):
            event.render(draw, font)

        for x in range(7):
            header = headers[x]
            tl = self._top_left + \
                Point(x * self._day_box.x + self._day_box.x / 2,
                      -10)
            s = Point(draw.textsize(header, font=font)[0] / 2,
                      0)

            draw.text(
                (tl - s).to_point(),
                header,
                font=font)


class Test(unittest.TestCase):

    def testName(self):
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
