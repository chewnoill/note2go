'''
Created on Sep 2, 2016

@author: will
'''
from PIL import Image, TiffImagePlugin, ImageFilter
import unittest
import qrtools
import tempfile
import os


def DecodeTiff(image):
    ret = {}
    with open(image, 'rb') as f:
        tiff = Image.open(f)
        if isinstance(tiff, TiffImagePlugin.TiffImageFile):
            temp_folder = tempfile.mkdtemp(prefix='tiff_decoder')
            n_frames = int(tiff.n_frames)
            tiff = Image.open(f)
            for page_num in range(n_frames):
                tiff.seek(page_num)
                print(tiff.tell())
                temp_file = os.path.join(temp_folder, 'temp%s.png' % page_num)
                print(temp_file)
                tiff.save(temp_file)
                r = DecodeImage(temp_file)
                for key in r:
                    ret[key] = {'image': r[key]['image'],
                                'image_path': temp_file}

        else:
            raise Exception('Expected tiff image')
    return ret


def DecodeImage(img_path):

    print(img_path)
    qr = qrtools.QR(filename=img_path)
    temp_dir = tempfile.gettempdir()
    if not qr.decode():
        # img blur to hide scanner artifacts

        temp_file = os.path.join(temp_dir, 'temp.png')

        im = Image.open(img_path)
        print(im.mode)
        im2 = im.convert('RGB')
        im1 = im2.filter(ImageFilter.GaussianBlur(radius=2))
        im1 = im1.filter(ImageFilter.SHARPEN)
        im1.save(temp_file)
        # im1.show()
        qr = qrtools.QR(filename=temp_file)
        if not qr.decode():
            return {}
    location = qr.location

    # size = (location[2][0] - location[0][0],
    #        location[2][1] - location[0][1])
    #size_m = max(size)
    #size = (size_m, size_m)

    size = (location[2][0] - location[0][0],
            location[2][1] - location[0][1])
    print(location)
    print(size)
    data = qr.data

    image = Image.open(img_path)
    qr = qrtools.QR(
        data=data,
        pixel_size=1,
        margin_size=1
    )
    qr_file = os.path.join(temp_dir, 'qr_code')
    qr.encode(qr_file)
    qr_img = Image.open(qr.filename)
    # print(qr_img.size)

    print('qr size: %s:%s' % qr_img.size)
    scale = (size[0] / float(qr_img.size[0]),
             size[1] / float(qr_img.size[1]))

    print('scale: %s:%s' % scale)
    offset_location = [(int(location[0][0] - scale[0]), int(location[0][1] - scale[0])),  # top left
                       (int(location[2][0] + scale[1]), int(location[2][1] + scale[1]))]  # bottom right
    offset_size = ((offset_location[1][0] - offset_location[0][0]),
                   (offset_location[1][1] - offset_location[0][1]))
    qr_img = qr_img.resize(offset_size)
    # image.show()
    image.paste(qr_img,
                offset_location[0])
    image.save(img_path)
    # image.show()
    return {qr.data: {'image': image,
                      'image_path': img_path}}


def Decode(path):
    image = Image.open(path)
    if isinstance(image, TiffImagePlugin.TiffImageFile):
        return DecodeTiff(path)
    else:
        return DecodeImage(path)


class Test(unittest.TestCase):

    def testImage(self):
        ret = Decode('/tmp/tiff_decoderTdWKZe/temp1.png')
        print(ret)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
