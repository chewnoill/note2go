'''
Created on Aug 29, 2016

@author: will
'''
import unittest

from note import Point


class Day:

    def __init__(self, tl, size, date=None, color=0xff):
        self._lines = []
        self.tl = tl
        self.br = tl + size
        self.fill = color

        self.date = date
        self.event_stack = []

    def addEvent(self, event, position):
        if position >= len(self.event_stack):
            diff = position + 1 - len(self.event_stack)
            self.event_stack += [None] * diff

        if self.event_stack[position]:
            print('event: %s' % event.raw['summary'])
            print('day: %s - %s' % (self.date, position))
            raise Exception('stack collision')

        print('safe event: %s' % event.raw['summary'])
        print('safe day: %s - %s' % (self.date, position))
        self.event_stack[position] = event

    def render(self, draw, font):
        draw.rectangle([self.tl.to_point(),
                        self.br.to_point()],
                       fill=self.fill,
                       outline=0)

        draw.text(
            (self.tl + Point(2, 2)).to_point(),
            '%s' % self.date.day,
            font=font)

        if len(self.event_stack) > 4:
            draw.text(
                (self.tl + Point(2, 12 * 6)).to_point(),
                '%s more...' % (len(self.event_stack) - 4),
                font=font)
        pass


class Event:

    def __init__(self, raw, days):
        self.raw = raw
        # register events with calendar
        self.start_day = days[0]
        self.end_day = days[-1]
        self.pos = self.stack_event(days)
        for day in days:
            day.addEvent(self, self.pos)

    def render(self, draw, font):
        if self.pos > 4:
            return
        tl = self.start_day.tl + Point(2, 12 + self.pos * 12)
        br = Point(self.end_day.br.x - 2, tl.y + 10)
        print('render event: %s' % self.raw['summary'])

        if 'date' in self.raw['start']:
            draw.rectangle([tl.to_point(),
                            br.to_point()],
                           fill=0xff,
                           outline=0)
        summary = self.raw['summary']
        elipse = ''
        while draw.textsize(summary + elipse, font=font)[0] > (br.x - tl.x):
            s = draw.textsize(summary + elipse, font=font)
            summary = summary[:-1]
            elipse = '...'
        draw.text(
            (tl + Point(2, 0)).to_point(),
            summary + elipse,
            font=font)

    def stack_event(self, days):
        stack_num = None
        day = days[0]

        def isFree(day, num):
            if (len(day.event_stack) > num and
                    day.event_stack[num]):
                return False
            else:
                return True

        for w in range(len(day.event_stack)):
            evt = day.event_stack[w]
            if evt:
                continue
            stack_num = w
            for x in range(len(days[1:])):
                nday = days[x]
                if isFree(nday, w):
                    continue
                stack_num = None
                break

            if stack_num is None:
                break
        print('event stack: %s' % day.event_stack)
        if stack_num is None:
            w = max(len(day.event_stack) - 1, 0)
            while True:
                print(w)
                stack_num = w
                for x in range(len(days[0:])):
                    nday = days[x]
                    if isFree(nday, w):
                        print('free: %s/%s' % (x, len(days[0:]) - 1))
                        continue
                    stack_num = None
                    break
                if stack_num is not None:
                    break
                w += 1
        return stack_num


class Test(unittest.TestCase):

    def testName(self):
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
