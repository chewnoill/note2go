'''
Created on Sep 7, 2016

@author: will
'''

from __future__ import print_function

import httplib2
import os

import oauth2client
from oauth2client import client
from oauth2client import tools
import oauth2client.file

import sys
try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser])
    flags.add_argument('--phase-two')
    flags = flags.parse_args()
    flags.noauth_local_webserver = True
except ImportError:
    flags = None

SCOPES = ['https://www.googleapis.com/auth/calendar.readonly',
          'https://www.googleapis.com/auth/gmail.readonly',
          'https://www.googleapis.com/auth/gmail.modify',
          'https://www.googleapis.com/auth/drive']


CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Calendar API Python Quickstart'

_GO_TO_LINK_MESSAGE = """
Go to the following link in your browser:

    {address}
"""


def main():
    """Shows basic usage of the Google Calendar API.

    Creates a Google Calendar API service object and outputs a list of the next
    10 events on the user's calendar.
    """
    credentials = get_credentials(code=flags.phase_two)
    http = credentials.authorize(httplib2.Http())


def get_credentials(code=None):
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    credential_dir = os.path.join('.', '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'credentials.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if not code:
            run_flow_phase_1(flow, store, flags)
            return
        else:
            run_flow_phase_2(flow, store, code, flags)
        print('Storing credentials to ' + credential_path)
    return credentials


def run_flow_phase_1(flow, storage, flags=None, http=None):
    print('running phase 1')
    oauth_callback = client.OOB_CALLBACK_URN
    flow.redirect_uri = oauth_callback
    authorize_url = flow.step1_get_authorize_url()

    print(_GO_TO_LINK_MESSAGE.format(address=authorize_url))
    sys.stdout.flush()


def run_flow_phase_2(flow, storage, code, flags=None, http=None):
    print('running phase 2')
    try:
        credential = flow.step2_exchange(code, http=http)
    except client.FlowExchangeError as e:
        sys.exit('Authentication has failed: {0}'.format(e))

    storage.put(credential)
    credential.set_store(storage)
    print('Authentication successful.')

    return credential

if __name__ == '__main__':
    main()
